%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% code to input some depth (h) and some wavenumber (k)
% and out put the corresponding freuqency f.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all 
close all 
clc

addpath(genpath(fullfile('functions','khfunctions')));

h=35;
g=9.81;
k0=5;
omega=sqrt(abs(g*k0*tanh(k0*h)))
kh=k0*h
f=omega/(2*pi)
% 
% kaxis=zeros(nt+1,1);
% tol=0.0001;
% tic
% for j=1:nt+1
% 	w0=omegaxis(end+1-j);
% 	k0_deep=w0^2/g;
% 	kaxis(j) = nr_k0(k0_deep,w0,h,g,tol); 
% end
% toc