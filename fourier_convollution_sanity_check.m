%%% sanity check convolution stuff %%%
clc 
clear all
close all

%%
L=0.2;
l0=1/L;	%%carrier dimensional frequency
k=l0./2.*pi;
xlen=500;
res=14;
x=linspace(-xlen,xlen,2^res);	%%time vector. 13 works
sideb=0.1;	%sideband fraction
deltak=0.1.*k;

env=1;	%%envelope
% env=exp(-x.^2);
% env=sech(x-10).*sech(x+10);
%%function
g=env.*(exp(1i.*k.*x)+sideb.*exp(1i.*(k+deltak).*x)+sideb.*exp(1i.*(k-deltak).*x));
g=hilbert(env.*(exp(1i.*k.*x)+sideb.*exp(1i.*(k+deltak).*x)+sideb.*exp(1i.*(k-deltak).*x)));

dx=x(2)-x(1);
dg=g(2:end)-g(1:end-1);
Dg=dg./dx;%% derivative of g
nx=length(x);



fou= fftshift(fft(g));%%fourier of g shifted for plot
Dfou= fftshift(fft(Dg));%%foudier of dg shifted for plot

kappaxis = 2*pi.*linspace(-1/dx/2,1/dx/2,nx); %fourier axis
% kappaxis = kappaxis(1:end-1);

Rg=ifft(fft(g));%%recover g
RDg=ifft(fft(Dg));%%recover g
% RDg_analitical=ifft(-1i.*(kappaxis).*fft(g));

RDg_analitical=ifft(ifftshift(1i.*(kappaxis).*fftshift(fft(g)))); %%analytical calculation of the derivative of g  F^-1[ iw F[g]]
% RDg_analitical2=ifft(1i.*(kappaxis+k).*fftshift(fft(g)));
RDfou= fftshift(fft(RDg_analitical));%./(k);%%foudier of  analytical dg

%%Hilbert transforms check
Hg=ifft(ifftshift(-1i.*sign(kappaxis).*fftshift(fft(g)))); %%H[g] 
HDg=ifft(-1i.*sign(kappaxis(1:end-1)).*fft(Dg));  %%H[dg/dx]
Hg2=ifft(-1i.*sign(kappaxis+k).*(fft(g)));


plotwin=10.*2.*pi./(k+deltak);

plot_test_functions_kappaxis(x,kappaxis,nx,k,deltak,plotwin,g,Hg,Dg,HDg,Rg,RDg,RDg_analitical,fou,Dfou,RDfou)

%%
kx1 = linspace(1,nx/2 ,nx/2)';
kx2 = linspace(-nx/2,-1,nx/2)';
kx = ((2*pi/dx/nx)*[kx1; kx2])';
kx=fftshift(kappaxis);
Hg_kx=ifft(-1i.*sign(kx).*(fft(g))); %%H[g] 
HDg_kx=ifft(-1i.*sign(kx(1:end-1)).*fft(Dg));  %%H[dg/dx]
RDg_analitical_kx=ifft((1i.*(kx).*(fft(g)))); %%analytical calculation of the derivative of g  F^-1[ iw F[g]]


figure('position',[20 0 1000 600])
subplot(221)
hold on
plot(x,real(g),'b','DisplayName','$g(t)$')
plot(x,abs(Hg),':b','LineWidth',1.5,'DisplayName','$|H[g]| shift$')
plot(x,abs(Hg_kx),'.-b','LineWidth',1.5,'DisplayName','$|H[g]| $')

% plot(x,abs(Hg2),'.-b','LineWidth',1.5,'DisplayName','$|H[g]| no shift$')
ylabel('g')
yyaxis right
plot(x(1:end-1),real(Dg),'r','DisplayName','$\frac{dg}{dx}$')
plot(x(1:end-1),abs(HDg),':r','LineWidth',1.5,'DisplayName','$|H[\frac{dg}{dx}]|$')
plot(x(1:end-1),abs(HDg_kx),'.-r','LineWidth',1.5,'DisplayName','$|H[\frac{dg}{dx}]| kx$')

hold off
% xlim([-plotwin,plotwin])
legend('interpreter','latex')
title('functions')
xlabel('x')

subplot(222)
hold on
plot(x,real(Rg),'b','DisplayName','$F^{-1}[F[g(x)]]$')
ylabel('g')
yyaxis right
plot(x(2:end),real(RDg),'r','DisplayName','$F^{-1}[F[\frac{dg}{dx}]]$')
plot(x,real(RDg_analitical),':r','LineWidth',1,'DisplayName','$F^{-1}[-i\omega F[g(x)]] shift$')
plot(x,real(RDg_analitical_kx),'.-r','LineWidth',1,'DisplayName','$F^{-1}[-i\omega F[g(x)]]$')

% plot(x,real(RDg_analitical2),'-.r','LineWidth',1,'DisplayName','$F^{-1}[-i\omega F[g(x)]] no shift$')

hold off
title('reconstruction')
xlim([-plotwin,plotwin])
xlabel('x')
legend('interpreter','latex')

%% sanitity check fourier dysthe


figure('position',[20 0 1000 600])
subplot(221)
hold on
plot(x,real(g),'b','DisplayName','$g(t)$')
plot(x,abs(Hg),':b','LineWidth',1.5,'DisplayName','$|H[g]| shift$')
% plot(x,abs(Hg2),'.-b','LineWidth',1.5,'DisplayName','$|H[g]| no shift$')
ylabel('g')
yyaxis right
plot(x(1:end-1),real(Dg),'r','DisplayName','$\frac{dg}{dx}$')
plot(x(1:end-1),abs(HDg),':r','LineWidth',1.5,'DisplayName','$|H[\frac{dg}{dx}]|$')
hold off
xlim([-plotwin,plotwin])
legend('interpreter','latex')
title('functions')
xlabel('x')


%% derivative RDg_analitical hilbert test. 
RDg_analitical2=ifft(ifftshift(1i.*(kappaxis).*fftshift(fft(abs(g).^2)))); %%analytical a_x |u|^2
Hg2_deriv=ifft(ifftshift(-1i.*sign(kappaxis).*(fftshift(fft(RDg_analitical2))))); %hilber of a_x |u|^2
Hg2_analit=ifft(ifftshift(1.*abs(kappaxis).*(fftshift(fft(abs(g).^2))))); %hilber of  |u|^2
Hg2_analit_k=ifft(ifftshift(1.*abs(kappaxis-k).*(fftshift(fft(abs(g).^2))))); %hilber of a_x |u|^2

figure()
hold on
plot(x,abs(Hg2_deriv),'k','LineWidth',1.5,'DisplayName','$H(a_x |U|^2)$')
plot(x,abs(Hg2_analit),':r','LineWidth',1.5,'DisplayName','$F^{-1}[-i\omega F[|U|^2]]$')
plot(x,abs(Hg2_analit_k),'b','LineWidth',1.5,'DisplayName','$F^{-1}[-i\omega F[|U|^2]]$ with k')
hold off
xlim([-plotwin,plotwin])
legend('interpreter','latex')
title('functions')
xlabel('x')


%% convolution test
figure()
filename_gif = 'fourier_test.gif';
pind=1;
for ee=0.001:0.07:4
h=exp(ee)-1;

% h=0.1;

	f=-1i.*(kappaxis)./tanh(h.*(kappaxis));
	f_kx=-1i.*(kx)./tanh(h.*(kx));

	Hil=-1i*abs(kappaxis);
	fc=-1i.*abs(kappaxis)./tanh(h.*(kappaxis));
	fc_kx=-1i.*abs(kx)./tanh(h.*(kx));
	hg=hilbert(real(g));
	fou_hg=fftshift(fft(hg));
	fou_hg2=fftshift(fft(abs(hg).^2));
	fou_hg2_kx=(fft(abs(hg).^2));


	%multpl:
	fxfou=f.*fou_hg;
	fxfou2=f.*fou_hg2;

	hilxfou=Hil.*fou_hg;
	fcxfou=fc.*fft(hg)/nx;
	spc_fxfou_kx=ifft((f_kx.*fft(hg.^2)));

	spc_fxfou=ifft(ifftshift(fxfou));
	spc_fxfou2=ifft(ifftshift(fxfou2));

	spc_fcxfou=ifft(fcxfou);
	spc_hilxfou=ifft(ifftshift(hilxfou));


	subplot(311)
	plot(kappaxis,abs(fou_hg)/nx,'b','DisplayName','$F[g]$');

	% plot(kappaxis,fc,':k','DisplayName','$f(k)$');
	yyaxis right
	plot(kappaxis,abs(f),'k','DisplayName','$f(k-w_0)$');
	ylim([0.8,1.2])

	xlim([k-3*deltak,k+3*deltak])
	xlabel(['1/m'])
	title(strcat('Hilber term test; coth(kh)  $$h = $$',num2str(h)),'interpreter','latex');
	legend('interpreter','latex')

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subplot(335)
	plot(x,h.*abs(hg.*spc_fxfou2),'k','LineWidth',1,'DisplayName','$$F^{-1}[f.F[g]]$$')
	xlim([-plotwin,plotwin])
	legend('interpreter','latex')
	title('our kappaxis')
	xlabel(['x'])

	subplot(338)
	plot(x,h.*abs(hg.*spc_fxfou_kx),'-.k','LineWidth',1,'DisplayName','$$F^{-1}[f.F[g]] kx$$')
	xlim([-plotwin,plotwin])
	legend('interpreter','latex')
	title('our kx')
	xlabel(['x'])

	subplot(334)
	plot(x,h.*abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'r','LineWidth',1.5,'DisplayName','$H[\partial_x|U|^2]$')
	% plot(x,spc_fcxfou,'r','DisplayName','$Fc^{-1}[f.F[g]]$')
	title('Dysthe kappaxis')
	xlim([-plotwin,plotwin])
	legend('interpreter','latex')
	xlabel(['x'])

	subplot(337)
	plot(x,h.*abs(hg.*ifft((abs(kx).*fft(abs(hg).^2)))),'-.r','LineWidth',1.5,'DisplayName','$H[\partial_x|U|^2]$')
	% plot(x,spc_fcxfou,'r','DisplayName','$Fc^{-1}[f.F[g]]$')
	title('Dysthe kx')
	xlim([-plotwin,plotwin])
	legend('interpreter','latex')
	xlabel(['x'])

	subplot([336])
	plot(x,(1./h).*abs(hg.*abs(hg).^2)+abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'b','LineWidth',1.5,'DisplayName','Simple Approximation')
	xlim([-plotwin,plotwin])
	xlabel(['x'])
	title('simple approximation')
	% ylim([-1.5,1.5])
	legend('interpreter','latex')

	subplot([339])
	plot(x,abs(hg.*abs(hg).^2),'b','LineWidth',1.5,'DisplayName','$U|U|^2$')
	xlim([-plotwin,plotwin])
	xlabel(['x'])
	title('nonlinear')
	% ylim([-1.5,1.5])
	legend('interpreter','latex')
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% subplot(313)
	% hold on
	% plot(x,abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'r','DisplayName','$H[\partial_x|U|^2]$')
	% plot(x,abs(hg.*abs(hg).^2),'b','DisplayName','$U|U|^2$')
	% yyaxis right
	% plot(x,abs(hg.*spc_fxfou2),'k','DisplayName','$$1/h F^{-1}[\frac{|k|}{\tanh{kh}}.F[g]]$$')
	% hold off
	% xlim([-plotwin,plotwin])
	% legend('interpreter','latex')



	drawnow
			pind=pind+1; %for the gif

		F = getframe(gcf);
        im = frame2im(F);
        [imind,cm] = rgb2ind(im,256);
		if pind == 2;
           imwrite(imind,cm,filename_gif,'gif', 'DelayTime',.12,'Loopcount',inf); 
        else         
           imwrite(imind,cm,filename_gif,'gif','WriteMode','append','DelayTime',.12);
		end
end


%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% plot for thesis 
addpath(genpath(fullfile('functions','plot')));


h=1;
kh=k*h;
% h=0.1;

	f=-1i.*(kappaxis)./tanh(h.*(kappaxis));
	f_kx=-1i.*(kx)./tanh(h.*(kx));

	Hil=-1i*abs(kappaxis);
	fc=-1i.*abs(kappaxis)./tanh(h.*(kappaxis));
	fc_kx=-1i.*abs(kx)./tanh(h.*(kx));
	hg=hilbert(real(g));
	fou_hg=fftshift(fft(hg));
	fou_hg2=fftshift(fft(abs(hg).^2));
	fou_hg2_kx=(fft(abs(hg).^2));


	%multpl:
	fxfou=f.*fou_hg;
	fxfou2=f.*fou_hg2;

	hilxfou=Hil.*fou_hg;
	fcxfou=fc.*fft(hg)/nx;
	spc_fxfou_kx=ifft((f_kx.*fft(hg.^2)));

	spc_fxfou=ifft(ifftshift(fxfou));
	spc_fxfou2=ifft(ifftshift(fxfou2));

	spc_fcxfou=ifft(fcxfou);
	spc_hilxfou=ifft(ifftshift(hilxfou));


	
	
ratio=20/9;
r1=400;
figure('position',[20 0 ratio*r1 1.3*r1])
set_latex_plots(groot)
Fsz=16;


% title(strcat('Hilber term test; coth(kh)  $$h = $$',num2str(h)),'interpreter','latex')
	subplot(221)
	plot(x,h.*abs(hg.*spc_fxfou2),'b','LineWidth',1.5,'DisplayName','$$|F^{-1}[coth(kh).F[\partial_{\xi}|U|^2]]|$$')
	xlim([-plotwin,plotwin])
	legend('interpreter','latex')
	title('Our model','FontSize',Fsz)
	xlabel(['$\xi$'],'FontSize',Fsz)
title(strcat('Meanflow term  $$kh = $$',num2str(kh)),'interpreter','latex')


	subplot(222)
	plot(x,h.*abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'k','LineWidth',1.5,'DisplayName','$|H[\partial_{\xi}|U|^2]|$')
	% plot(x,spc_fcxfou,'r','DisplayName','$Fc^{-1}[f.F[g]]$')
	title('Dysthe Hilbert','FontSize',Fsz)
	xlim([-plotwin,plotwin])
	legend('interpreter','latex')
	xlabel(['$\xi$'],'FontSize',Fsz)

	subplot([223])
	plot(x,(1./h).*abs(hg.*abs(hg).^2)+abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'k','LineWidth',1.5,'DisplayName','|Simple Approximation|')
	xlim([-plotwin,plotwin])
	xlabel(['$\xi$'],'FontSize',Fsz)
	title('Simple approximation','FontSize',Fsz)
	% ylim([-1.5,1.5])
	legend('interpreter','latex')

	subplot([224])
	plot(x,abs(hg.*abs(hg).^2),'k','LineWidth',1.5,'DisplayName','$|U|U|^2|$')
	xlim([-plotwin,plotwin])
	xlabel(['$\xi$'],'FontSize',Fsz)
	title('Non-linear term','FontSize',Fsz)
	% ylim([-1.5,1.5])
	legend('interpreter','latex')

	
	%%%%%%%%%%
	
	
	
%%	

kh=1000;
h=kh/k;
% h=0.1;

	f=-1i.*(kappaxis)./tanh(h.*(kappaxis));
	f_kx=-1i.*(kx)./tanh(h.*(kx));

	Hil=-1i*abs(kappaxis);
	fc=-1i.*abs(kappaxis)./tanh(h.*(kappaxis));
	fc_kx=-1i.*abs(kx)./tanh(h.*(kx));
	hg=hilbert(real(g));
	fou_hg=fftshift(fft(hg));
	fou_hg2=fftshift(fft(abs(hg).^2));
	fou_hg2_kx=(fft(abs(hg).^2));


	%multpl:
	fxfou=f.*fou_hg;
	fxfou2=f.*fou_hg2;

	hilxfou=Hil.*fou_hg;
	fcxfou=fc.*fft(hg)/nx;
	spc_fxfou_kx=ifft((f_kx.*fft(hg.^2)));

	spc_fxfou=ifft(ifftshift(fxfou));
	spc_fxfou2=ifft(ifftshift(fxfou2));

	spc_fcxfou=ifft(fcxfou);
	spc_hilxfou=ifft(ifftshift(hilxfou));

ratio=20/9;
r1=400;
figure('position',[20 0 ratio*r1 1.3*r1])
set_latex_plots(groot)
Fsz=16;

% title(strcat('Hilber term test; coth(kh)  $$h = $$',num2str(h)),'interpreter','latex')
% subplot(221)
hold on
plot(x,abs(hg.*abs(hg).^2),'--k','LineWidth',2,'DisplayName','$|U|U|^2|$')
plot(x,abs(hg.*spc_fxfou2),'b','LineWidth',1.5,'DisplayName','$$|F^{-1}[\coth(kh).F[\partial_{\xi}|U|^2]]|$$')
plot(x,((1./h).*abs(hg.*abs(hg).^2)+abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2)))),':r','LineWidth',1.3,'DisplayName','$|$Simple Approximation$|$')
plot(x,abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),':k','LineWidth',2,'DisplayName','$|H[\partial_{\xi}|U|^2]|$')
xlim([-plotwin,plotwin])
legend('interpreter','latex')
title('Our model','FontSize',Fsz)
xlabel(['$\xi$'],'FontSize',Fsz)
title(strcat('Meanflow comparison $$kh = $$',num2str(kh)),'interpreter','latex')
hold off
ylabel(['Amplitude $[\mathrm{m}^3]$'],'FontSize',Fsz)
set(gca,'fontsize',Fsz-1)

% 	subplot(222)
% 	plot(x,h.*abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'k','LineWidth',1.5,'DisplayName','$H[\partial_{\xi}|U|^2]$')
% 	% plot(x,spc_fcxfou,'r','DisplayName','$Fc^{-1}[f.F[g]]$')
% 	title('Dysthe Hilbert','FontSize',Fsz)
% 	xlim([-plotwin,plotwin])
% 	legend('interpreter','latex')
% 	xlabel(['$\xi$'],'FontSize',Fsz)
% 
% 	subplot([223])
% 	plot(x,(1./h).*abs(hg.*abs(hg).^2)+abs(hg.*ifft(ifftshift(abs(kappaxis).*fou_hg2))),'k','LineWidth',1.5,'DisplayName','Simple Approximation')
% 	xlim([-plotwin,plotwin])
% 	xlabel(['$\xi$'],'FontSize',Fsz)
% 	title('Simple approximation','FontSize',Fsz)
% 	% ylim([-1.5,1.5])
% 	legend('interpreter','latex')
% 
% 	subplot([224])
% 	plot(x,abs(hg.*abs(hg).^2),'k','LineWidth',1.5,'DisplayName','$U|U|^2$')
% 	xlim([-plotwin,plotwin])
% 	xlabel(['$\xi$'],'FontSize',Fsz)
% 	title('Non-linear term','FontSize',Fsz)
% 	% ylim([-1.5,1.5])
% 	legend('interpreter','latex')
	
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% convolution kinad working test
% 
% % f=exp(-(kappaxis-k).^2);
% h=1;
% f=-1i.*h.*abs(kappaxis-k)./tanh(h.*(kappaxis-k));
% Hil=-1i*sign(kappaxis-k);
% fc=-1i.*h.*abs(kappaxis)./tanh(h.*(kappaxis));
% hg=hilbert(g);
% fou_hg=fftshift(fft(hg));
% figure()
% subplot(311)
% % plot(x,f,x,g)
% hold on
% plot(kappaxis,abs(f),'DisplayName','$f(k-w_0)$');
% plot(kappaxis,fc,'DisplayName','$f(k)$');
% % plot(kappaxis,abs(fft(g))/nx,'DisplayName','$fg$');
% plot(kappaxis,abs(fou_hg)/nx,'DisplayName','$F[g]$');
% xlim([k-3*deltak,k+3*deltak])
% hold off
% xlabel(['Hz'])
% legend('interpreter','latex')
% ylim([0,2])
% title('Hilber term test')
% 
% %multpl:
% fxfou=f.*fou_hg;
% hilxfou=Hil.*fou_hg;
% fcxfou=fc.*fft(hg)/nx;
% spc_fxfou=ifft(ifftshift(fxfou));
% spc_fcxfou=ifft(fcxfou);
% spc_hilxfou=ifft(ifftshift(hilxfou));
% 
% subplot(312)
% hold on
% plot(x,abs(hg.*spc_fxfou),'DisplayName','$$F^{-1}[f.F[g]]$$')
% % plot(x,spc_fcxfou,'DisplayName','$Fc^{-1}[f.F[g]]$')
% plot(x,abs(hg),'DisplayName','$g$')
% hold off
% xlim([-plotwin,plotwin])
% xlabel(['x'])
% % ylim([-1.5,1.5])
% legend('interpreter','latex')
% 
% subplot(313)
% hold on
% plot(x,abs(hg.*spc_fxfou),'DisplayName','$$F^{-1}[f.F[g]]$$')
% plot(x,abs(hg.*abs(hg).^2),'DisplayName','$g|g|^2$')
% hold off
% xlim([-plotwin,plotwin])
% legend('interpreter','latex')
% 

function plot_test_functions_kappaxis(x,kappaxis,nx,k,deltak,plotwin,g,Hg,Dg,HDg,Rg,RDg,RDg_analitical,fou,Dfou,RDfou)

figure('position',[20 0 1000 600])
subplot(221)
hold on
plot(x,real(g),'b','DisplayName','$g(t)$')
plot(x,abs(Hg),':b','LineWidth',1.5,'DisplayName','$|H[g]| shift$')
% plot(x,abs(Hg2),'.-b','LineWidth',1.5,'DisplayName','$|H[g]| no shift$')
ylabel('g')
yyaxis right
plot(x(1:end-1),real(Dg),'r','DisplayName','$\frac{dg}{dx}$')
plot(x(1:end-1),abs(HDg),':r','LineWidth',1.5,'DisplayName','$|H[\frac{dg}{dx}]|$')
hold off
xlim([-plotwin,plotwin])
legend('interpreter','latex')
title('functions')
xlabel('x')

subplot(222)
hold on
plot(x,real(Rg),'b','DisplayName','$F^{-1}[F[g(x)]]$')
ylabel('g')
yyaxis right
plot(x(2:end),real(RDg),'r','DisplayName','$F^{-1}[F[\frac{dg}{dx}]]$')
plot(x,real(RDg_analitical),':r','LineWidth',1,'DisplayName','$F^{-1}[-i\omega F[g(x)]] shift$')
% plot(x,real(RDg_analitical2),'-.r','LineWidth',1,'DisplayName','$F^{-1}[-i\omega F[g(x)]] no shift$')

hold off
title('reconstruction')
xlim([-plotwin,plotwin])
xlabel('x')
legend('interpreter','latex')

subplot(223)
plot(kappaxis,abs(fou)/nx,'b','DisplayName','$F[g(x)]$');
xlim([k-3*deltak,k+3*deltak])
title('fourier')
legend('interpreter','latex')
xlabel('k')

subplot(224)
hold on
plot(kappaxis(2:end),abs(Dfou)/nx,'r','DisplayName','$F[\frac{dg}{dx}]$');
plot(kappaxis,abs(RDfou)/nx,':r','LineWidth',1,'DisplayName','$F[F^{-1}[-i\omega F[g(x)]]]$');
hold off
title('fourier')
legend('interpreter','latex')
xlim([k-3*deltak,k+3*deltak])
xlabel('k')


end





%%%%%%%%%%%%

function kfin = nr_k0(k0,w0,h,g,tol)

	function val=nr_k(k0,w0,g,h)
		sigma=tanh(k0*h);
		val=  w0^2/g-k0*sigma;
	end

	function val_prime = nr_k_prime(k0,h)
	   sigma=tanh(k0*h);
	   val_prime= -( sigma+k0*h*(1-sigma^2) );
	end

	min_it=10;
	it=1;
	while it<min_it %DO AT LEAST "min_it" ITERATIONS
		k0 = k0-nr_k(k0,w0,g,h)./nr_k_prime(k0,h);
		it=it+1;
	end
	
	while nr_k(k0,w0,g,h)>tol %Continue until beyond tol
        k0 = k0-nr_k(k0,w0,g,h)./nr_k_prime(k0,h);
		it=it+1;
	end
	
% 	print(k0)
	kfin=k0;
	
end