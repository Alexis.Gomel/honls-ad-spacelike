function [params]=case2_param_dialog()

	prompt = {'\Omega [m^{-1/2}]','L_\xi [m]','L_\tau (time window, periods/soliton length) [ad]',...
		'n_t (log_2)','Number of steps to store N_x','\epsilon (steepness = k_0 a_0) [ad]',...
		'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
	dlg_title = 'Parameters';
	options.Interpreter = 'tex';
	num_lines = 1;
	if ~exist('button_dialog_temp_2.txt', 'dir') %check if dir already exists
		defaultans=importfile_buttondialog_case1('button_dialog_temp_2.txt');
		defaultans=cellstr(defaultans);
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	else
		defaultans = {'1','50','10','11','100','0.14','1','0','yes','1','0.0021072','100'};
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	end

end