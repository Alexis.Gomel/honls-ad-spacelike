function [params]=case1_param_dialog_nb()
%print dialog for no bathymetry codes
	prompt = {'f [1/s]','kh','Tmax [s]','L_\xi (space window, periods/soliton length) [ad]',...
		'n_x (log_2)','Number of steps to store N_t','\epsilon (steepness = k_0 a_0) [ad]',...
		'Export initial function','W-M Factor','Output time [s]'};
	dlg_title = 'Parameters';
	options.Interpreter = 'tex';
	num_lines = 1;
	
	try %This sets the option to the last file used.
		defaultans=importfile_buttondialog_case1('button_dialog_temp_1.txt');
		defaultans=cellstr(defaultans);
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	catch exception %in case this file doesn't exist, it uses another file.
		defaultans = {'1.01','1.3','1000','250','12','128','0.06','no','1.83','100'};
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	end


end