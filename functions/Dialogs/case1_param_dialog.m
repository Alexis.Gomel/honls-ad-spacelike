function [params]=case1_param_dialog()

	prompt = {'f [1/s]','Tmax [s]','L_\xi (space window, periods/soliton length) [ad]',...
		'n_x (log_2)','Number of steps to store N_t','\epsilon (steepness = k_0 a_0) [ad]',...
		'Shoaling ON/OFF','FOD ON/OFF','Export initial function','W-M Factor','Disipation','Output time [s]'};
	dlg_title = 'Parameters';
	options.Interpreter = 'tex';
	num_lines = 1;
	if ~exist('button_dialog_temp_1.txt', 'dir') %check if dir already exists
		defaultans=importfile_buttondialog_case1('button_dialog_temp_1.txt');
		defaultans=cellstr(defaultans);
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	else
		defaultans = {'1.01','10','120','12','100','0.06','1','1','no','1.83','0.00','100'};
		params = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	end

end