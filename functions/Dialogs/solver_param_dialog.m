function [solvpar]=solver_param_dialog()

	prompt = {'initial step','minimum step','tolerance'};
	dlg_title = 'Solver parameters';
	options.Interpreter = 'tex';
	num_lines = 1;
	defaultans = {'0.01','1e-6','1e-9'};
	solvpar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);

end

% Lx = 1000; % [m]
% number of points to save
% Nx = 250;
% hx0 = 0.01;
% hxmin = 1e-6;
% tol = 1e-10;