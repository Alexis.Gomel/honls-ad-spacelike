function [bathymetrypar]=bathymetry_dialog_prompt()

	prompt = {'hk_{0}','hk_{fin}','\xi_1','\xi_2','\xi_{slope}'};
	dlg_title = 'Solver parameters';
	options.Interpreter = 'tex';
	num_lines = 1;
	if ~exist('kh_dialog_temp.txt', 'dir') %check if dir already exists
		defaultans=importfile_buttondialog_kh('kh_dialog_temp.txt');
		defaultans=cellstr(defaultans);
		bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	else
		defaultans = {'24.146','24.146','13.5','15.5','20'};
		bathymetrypar = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
	end


end