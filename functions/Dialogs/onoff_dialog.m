function [NLS_coef]=onoff_dialog()
	
	titles={'NLS Dispersion','NLS Nonlinear','HO 3rd Dispersion','HO current b21','HO Asymmetry b22','Meanflow','HO 4th Dispersion'};
	dlg_title = 'Equation coeffiecients ON/OFF';
	options.Interpreter = 'tex';
	if ~exist('button_dialog_temp_1.txt', 'dir') %check if dir already exists
		def_coef_dialg=importfile_eq_coef('default_coef_equation_temp.txt');
		def_coef_dialg=cellstr(def_coef_dialg);
% 		fprintf('exist')
		NLS_coef=inputdlg(titles,dlg_title,1,def_coef_dialg);
	else
		def_coef_dialg = {'1','1','1','1','1','1','0'};
		NLS_coef=inputdlg(titles,dlg_title,1,def_coef_dialg);
	end

end