function [b21_colour, b22_colour,h3_colour,a3_colour]=color_language_HONLScoefs()
%this are the new colour mtlab colour defaults. 
	b21_colour=[27, 158, 119]./255; %beta 21
    b22_colour=[217, 95, 2]./255; %beta 22.
	h3_colour=[255, 0, 144]./255; %magenta h3
	a3_colour=[117, 112, 179]./255; %alpha 3 %magenta
end
	