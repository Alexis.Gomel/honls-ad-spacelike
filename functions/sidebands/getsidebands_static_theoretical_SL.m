function [ModesAmplitudes]=getsidebands_static_theoretical_SL(data_series,central_frequency,var,pos,Debug)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code searches for the first and second sidebands of a data series. (array)
% Inputs: 
%         - data is the data array for the data_series.
%         - data series array, is the array to extract the sideband
%         information from
%         - Aproximate central frequency
%         - Debug option: 1 to print debug plot, 0 if not.
%
% Outputs:
%         - ModesAmplitudes: Is an array with the complex value of the fft
%         main modes, up to the 2nd sidebands
%         - ModesPhases: Is an array with the Phases of the fft
%         main modes, up to the 2nd sidebands
%
% A.Gomel 15/01/2020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sz=size(data_series);
dsl=sz(1);


pp=0;
padding_points=(dsl+1)*2^pp-1; %Amount of padding.

foudata=2*fftshift(fft(data_series,padding_points,1),1)./dsl;
L=length(foudata);
hvar=var(2)-var(1);
new_omegaxis=pi.*linspace(-1/hvar/2,1/hvar/2,L)';
% posit=new_omegaxis>=0;

	if Debug==1
		figure()
		subplot(111)
		plot(new_omegaxis(:),abs(foudata(:,end)))
	end
	pomega=new_omegaxis%(posit);
	pfou=foudata%(posit,:);
	[psor,lsor] = findpeaks(abs(pfou(:,pos)),'SortStr','descend');%sorts the data
	ind1=lsor(1);
	ind2=lsor(2);
	ind3=lsor(3);
	max1=psor(1);
	max2=psor(2);
	max3=psor(3);

	if max1>=max2  %%%% this is to refer to the highest first sideband, just in case.
		ind_dist=abs(ind2-ind1);
		fmod= abs(central_frequency-pomega(ind2)); %modulation frequency

	elseif max1<max2
		ind_dist=abs(ind3-ind1);
		fmod= abs(central_frequency-pomega(ind3)); %modulation frequency
	end

	Tmod=1./fmod;  %aproximate modulation period
	if Debug==1
		figure()
			subplot(311)
			plot(pomega,real(pfou))
			hold on
			plot(pomega(ind1),max1,'bx')
			plot(pomega(ind2),max2,'bx')
			plot(pomega(ind3),max3,'bx')
			hold off
			xlim([0,4]);
			subplot(312)
			plot(pomega,abs(pfou))
			hold on
			plot(pomega(ind1),abs(pfou(ind1)),'gx')
			plot(pomega(ind1+ind_dist),abs(pfou(ind1+ind_dist)),'gx')
			plot(pomega(ind1-ind_dist),abs(pfou(ind1-ind_dist)),'gx')
			plot(pomega(ind1+2*ind_dist),abs(pfou(ind1+2*ind_dist)),'gx')
			plot(pomega(ind1-2*ind_dist),abs(pfou(ind1-2*ind_dist)),'gx')
			hold off
			xlim([0,4]);
			subplot(313)
			plot(pomega,mod(unwrap(angle(pfou)),2*pi))    
			hold on
			plot(pomega(ind1),mod(unwrap(angle(pfou(ind1))),2*pi),'gx')    
			plot(pomega(ind1+ind_dist),mod(unwrap(angle(pfou(ind1+ind_dist))),2*pi),'gx')    
			plot(pomega(ind1-ind_dist),mod(unwrap(angle(pfou(ind1-ind_dist))),2*pi),'gx')    
			hold off
			xlim([0,4]);
	end 

	L=length(pfou);                                          %data series lenght
	finalfoudata=pfou;   %/max(foudata(L/2+1:end));
	ModesAmplitudes=zeros(5,length(data_series(1,:)));                                %empty output matrix

	%%%%%%%%%%%% set ourput array to  a matrix %%%%%%%%%%
	ModesAmplitudes(1,:) = finalfoudata(ind1,:); %central mode
	ModesAmplitudes(2,:) = finalfoudata(ind1+ind_dist,:);   %right first sideband
	ModesAmplitudes(3,:) = finalfoudata(ind1-ind_dist,:);   %left first sideband
	ModesAmplitudes(4,:) = finalfoudata(ind1+2*ind_dist,:); %right second sideband
	ModesAmplitudes(5,:) = finalfoudata(ind1-2*ind_dist,:); %left second sideband
end