function kh = khfun3(x,kh0,khm,xvarstart,xvarstop,xslope)
    %%
    % Linear from hk0 to hkm and back
    % INPUT
    % x point in the propagation distance
    % kh0 initial/final kh
    % khm transition kh
    % xvarstart/stop initial/final coordinate of kh variation
    % xslope used for transition.
    % 
    % OUTPUT
    % kh(x)
    %%
%     xvarm = xvarstart + 0.5*(xvarstop-xvarstart);
    kh = zeros(size(x));
    kh(x<=xvarstart) = kh0;
    kh(x>xvarstop) = kh0;
    kh(x>xvarstart & x<=xvarstop) = khm;
    Dkh = khm-kh0;
    Dx = abs(Dkh.*xslope);
    kh(x>xvarstart & x<=xvarstart + Dx ) = kh0 + sign(Dkh)*(x(x>xvarstart & x<=xvarstart + Dx)-xvarstart)/xslope;
    kh(x>xvarstop - Dx & x<=xvarstop ) = kh0 - sign(Dkh)*(x(x>xvarstop - Dx & x<=xvarstop )-xvarstop)/xslope;

    
end