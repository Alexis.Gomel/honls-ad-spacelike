function f = plot_conservations_SL(X,N,P,Fsz)

f=figure('name','Conservations');
	set_latex_plots(groot)
	subplot(311);
	plot(X,N./N(1),'linewidth',1.5);
	xlabel('$T$','fontsize',Fsz);
	set(gca,'FontSize',Fsz-1);
	ylabel('$\frac{N}{N_0}$','fontsize',Fsz);
	subplot(312);
	plot(X,P./P(1),'linewidth',1.5);
	set(gca,'FontSize',Fsz-1);
	xlabel('$T$','fontsize',Fsz);
	ylabel('$\frac{P}{P_0}$','fontsize',Fsz);
	subplot(313);
	plot(X(2:end),diff(real(P./N')),'linewidth',1.5);
	set(gca,'FontSize',Fsz-1);
	xlabel('$T$','fontsize',Fsz);
	ylabel('$\frac{d(P/N)}{dt}$','fontsize',Fsz);
	% suptitle('Conservation of momenta');

end