function f=plot_V_shock(K,H,X,Bback,Bpert,B0,t,u,tplot,Lx,xvarstart,xvarstop,cmap,omega,kh0,FOD,ON_OFF_coef)
	%%%ssetup
		crit= 1.363;
		g0=9.81;
		tp=t(tplot)./sqrt(g0);
		phase=unwrap(angle(u(tplot,:)));
		chirp=diff(phase)./(diff(t(tplot))');
		t_chirp_plot=t(tplot);
		%% Amplitude figure
		f=figure('name','Chirp Evolution','position',[20 20 550 550]);
		[ d, ix ] = min( abs( K.*H - crit ) );%find the critical value index
		pos1 = [0.05 0.5 0.9 0.65];
		ax1=subplot(5,1,[1 3]);
		hold on
		s2=pcolor(tp,X,abs(u(tplot,:))');        
		set(s2, 'EdgeColor', 'black','LineWidth',1.,'MeshStyle','None');
		title('Choose X position');

		s2.FaceColor = 'interp';
		colormap(cmap)
		xlim([tp(1),tp(end-1)])
		ylim([X(1), X(end)])
		ax_plt = gca;
		ax_plt_pos = ax_plt.Position ;
		start_end_pos = ginput(1); %%%%%%%%%   Input
		startpos_ti = start_end_pos(1,1);
	
		
		[ d, ix_y_right ] = min( abs( X - start_end_pos(1,2) ) );%find the critical value index
		[ d, ix_x_right ] = min( abs( t_chirp_plot(1:end-1)/sqrt(g0)- start_end_pos(1,1) ) );%find the critical value index
		x_init_right=X(ix_y_right);%%%%%%%% Position of shock
		plot(t_chirp_plot(ix_x_right)/sqrt(g0),x_init_right,'*r');
		hold off		
		
		

		%%%%%
		title('Choose right time');

		subplot(5,1,4);
		ax2 = gca;
		ax2_pos = ax2.Position;
		ax2_pos =  [ ax2_pos(1)  ax2_pos(2)  .84*ax_plt_pos(3)  ax2_pos(4)];
% 		set(ax2,'Position',ax2_pos);%set to same width as first plot
		hold on
		plot(tp,abs(u(tplot,ix_y_right)),'-b','LineWidth',1.3)
		hold off
		ylabel('$\rho$','Interpreter','latex','fontsize',13);
		xlabel('$\tau$ [s]','Interpreter','latex','fontsize',13);
		xlim([tp(1),tp(end-1)])

		
		%%%%


		subplot(5,1,5);
		ax2 = gca;
		ax2_pos = ax2.Position;
		ax2_pos =  [ ax2_pos(1)  ax2_pos(2)  .84*ax_plt_pos(3)  ax2_pos(4)];
% 		set(ax2,'Position',ax2_pos);%set to same width as first plot
		hold on
		plot(t_chirp_plot(1:end-1)/sqrt(g0),chirp(:,ix_y_right),'-b','LineWidth',1.3)
		hold off
		ylabel('$u$','Interpreter','latex','fontsize',13);
		xlabel('$\tau$ [s]','Interpreter','latex','fontsize',13);
		xlim([tp(1),tp(end-1)])

		%%%%%%
	

		start_end_pos = ginput(1);%% Right time input
		[ d, ix_x_right ] = min( abs( t_chirp_plot(1:end-1)/sqrt(g0)- start_end_pos(1,1) ) );%find the critical value index
		t_init_right=t_chirp_plot(ix_x_right)/sqrt(g0);
	
		
		[h,k,sigma,cg,cp,lambda,nu,alpha,alpha4,beta,gamma,mu,muintexp,Dys_hilb] = depthdependent_HONLSparameters_onoff(omega,kh0,FOD,ON_OFF_coef)

		

		axes(ax1)
		title('Choose left time');
		start_end_pos = ginput(1);
		[ d, ix_x_left ] = min( abs( t_chirp_plot(1:end-1)/sqrt(g0)- start_end_pos(1,1) ) );%find the critical value index
		t_init_left=t_chirp_plot(ix_x_left)/sqrt(g0); 
				
		rho_right=abs(u(ix_x_right,ix_y_right));
		u_right=chirp(ix_x_right,ix_y_right);
		rho_left=abs(u(ix_x_right,ix_y_right));
		u_left=chirp(ix_x_left,ix_y_right);
		
		U0_right=rho_right;%Amplitude
		U0_left=rho_left;
		T0=t_init_right-t_init_left; %pulse duration

		Znl_right=1/(nu.*U0_right.^2);
		Znl_left=1/(nu.*U0_left.^2);
		
		Zd=T0^2/(2*lambda);
		gamma_trillo=1;
		beta_2_trillo=1;
		beta_3_trillo_right=-6*alpha*sqrt(1/((2*abs(lambda)^3)*Znl_right));
		beta_3_trillo_left=-6*alpha*sqrt(1/((2*abs(lambda)^3)*Znl_left));

		gamma_trillo_mult_right=U0_right/(sqrt(2*abs(lambda*nu)));
		gamma_trillo_mult_left=U0_right/(sqrt(2*abs(lambda*nu)));

		gamma_21_trillo_right=beta*gamma_trillo_mult_right;
		gamma_21_trillo_left=beta*gamma_trillo_mult_left;

		gamma_22_trillo_right=gamma*gamma_trillo_mult_right;
		gamma_22_trillo_left=gamma*gamma_trillo_mult_left;

		a_11_right=beta_2_trillo.*u_right+beta_3_trillo_right./2.*u_right.^2+gamma_21_trillo_right.*(rho_right);
		a_11_left=beta_2_trillo.*u_left+beta_3_trillo_left./2.*u_left.^2+gamma_21_trillo_left.*(rho_left);


		
		
		V_right_plus=a_11_right+sqrt(rho_right*(gamma_trillo+(gamma_21_trillo_right-gamma_22_trillo_right)*u_right)*(beta_2_trillo+beta_3_trillo_right*u_right)+(gamma_22_trillo_right*rho_right).^2);
		V_right_minus=a_11_right-sqrt(rho_right*(gamma_trillo+(gamma_21_trillo_right-gamma_22_trillo_right)*u_right)*(beta_2_trillo+beta_3_trillo_right*u_right)+(gamma_22_trillo_right*rho_right).^2);

			
		V_left_plus=a_11_left+sqrt(rho_left*(gamma_trillo+(gamma_21_trillo_left-gamma_22_trillo_left)*u_left)*(beta_2_trillo+beta_3_trillo_left*u_left)+(gamma_22_trillo_left*rho_left).^2);
		V_left_minus=a_11_left-sqrt(rho_left*(gamma_trillo+(gamma_21_trillo_left-gamma_22_trillo_left)*u_left)*(beta_2_trillo+beta_3_trillo_left*u_left)+(gamma_22_trillo_left*rho_left).^2);

		
		%%% NLS version
% 		V_right_plus=u_right+rho_right;
% 		V_right_minus=u_right-rho_right;
% 
% 		V_left_plus=u_left+rho_left;
% 		V_left_minus=u_left-rho_left;

		%%%%%%
		axes(ax1)
		title('Shock soliton propagation');

		hold on
		plot(t_init_right,x_init_right,'*g');
		plot(t_init_right+V_right_plus.*(X(end)-x_init_right), X(end),'*g');
		plot([t_init_right t_init_right+V_right_plus.*(X(end)-x_init_right)],[x_init_right X(end)],'--r','LineWidth',1.8)
		plot([t_init_right t_init_right+V_right_minus.*(X(end)-x_init_right)],[x_init_right X(end)],':r','LineWidth',1.8)

		plot([t_init_left t_init_left+V_left_plus.*(X(end)-x_init_right)],[x_init_right X(end)],'--r','LineWidth',1.8)
		plot([t_init_left t_init_left+V_left_minus.*(X(end)-x_init_right)],[x_init_right X(end)],':r','LineWidth',1.8)

		hold off
		
end