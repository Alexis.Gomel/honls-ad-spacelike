function timeevolution(x,T,kappaxis,uloc,u0,L0,T0,a0,k0,units) 

nt = length(uloc);
switch units
    case 'Adimensional'        

        subplot(121)
        plot(x,abs(uloc),x,abs(u0));
        xlim([-10/T0,10/T0]);
        xlabel('\tau','fontsize',14); 
        set(gca,'fontsize',13);
        subplot(122)
        semilogy(kappaxis,abs(fftshift(ifft(uloc))).^2/nt.^2);xlabel('\kappa','fontsize',14);
        xlim([-10/L0,10/L0]);
        title(strcat('\tau = ',num2str(T)));
        set(gca,'fontsize',13);
        
    case 'Dimensional'
        
        subplot(121)
        plot(x,abs(uloc),x,abs(u0));
        
% 		plot(x*L0,abs(uloc).^2*a0^2,x*L0,abs(u0).^2*a0^2);
        xlabel('t [s]','fontsize',14);xlim([-10*L0,10*L0]);
        set(gca,'fontsize',13);
        subplot(122)
        semilogy(kappaxis,abs(fftshift(ifft(uloc))).^2/nt.^2);xlabel('\kappa','fontsize',14);
%         semilogy(kappaxis/L0/2/pi + k0,abs(fftshift(ifft(uloc))).^2/nt.^2*a0^2);
%         xlim([k0-1/L0,k0+1/L0]);
        xlabel('k [m^-1]','fontsize',14);
%         set(gca,'fontsize',13);
        title(strcat('t = ',num2str(T*T0)));
end