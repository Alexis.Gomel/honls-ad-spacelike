function [fig]=plot_parameterspace(kappaxis,Omega,kh0,ON_OFF_coef)

	[h0,k0,sigma0,cg0,cp0,alpha_hat,bhat_D,alpha_hat3,alpha40_hat,beta_hat21,beta_hat22,mu0,muintexp0,Hhat3] = depthdependent_HONLS_SpaceLike(Omega,kh0,ON_OFF_coef);
	khaxis=h0.*kappaxis(kappaxis>0);
	on=ones([1 length(khaxis)]);
	[H_ax,K_ax,Sigma_ax,Cg_ax,Cp_ax,Alpha_hat_ax,Bhat_D_ax,Alpha_hat3_ax,Alpha_hat4_ax,Beta_hat21_ax,Beta_hat22_ax,Mu_ax,Muintexp_ax,HHat3_ax] = depthdependent_HONLS_SpaceLike(Omega,khaxis,ON_OFF_coef);
	
	fig=figure('name','Parameters','position',[20 80 1250 500]);
	
	subplot(231)
	hold on
	plot(khaxis,Alpha_hat_ax,'b','Displayname','$$\hat{\alpha}$$')
	plot(kh0,alpha_hat,'*k','Displayname','None' )
	plot(khaxis,-on.*Omega./(8.*K_ax(end).^2),'--b','Displayname','$$\frac{-\omega }{8k_0^2}$$')
	ax = gca;
	ax.YColor = 'b';
	ylabel('$$\hat{\alpha}$$','interpreter','latex');	ytickformat('%,.2f');
	hold off
	yyaxis right
	hold on
	plot(khaxis,Bhat_D_ax,'r','Displayname','$$\hat{\beta}_D$$')
	plot(kh0,bhat_D,'*k','Displayname','None' )
	plot(khaxis,on.*Omega.*(K_ax(end).^2)./2,'--r','Displayname','$$\frac{\omega k_0^2}{2}$$')
	hold off
	ylabel('$$\hat{\beta}_D$$','interpreter','latex')
	xlabel('kh','interpreter','latex')
	% legend('interpreter','latex')
	xlim([0.5*kh0,kh0*1.5])
	ytickformat('%,.2f')
	set(gca,'ycolor','r') 

	subplot(232)
	hold on
	plot(khaxis,Beta_hat21_ax,'b','Displayname','$$\hat{\beta}_{21}$$')
	plot(kh0,beta_hat21,'*k','Displayname','None' )
	plot(khaxis,on.*3.*Omega.*K_ax(end)./2,'--b','Displayname','$$\frac{3\omega k_0}{2}$$')
	hold off
	ylabel('$$\hat{\beta}_{21}$$','interpreter','latex');	ytickformat('%,.2f');
	yyaxis right
	hold on
	plot(khaxis,Beta_hat22_ax,'r','Displayname','$$\hat{\beta}_{22}$$')
	plot(khaxis,on.*Omega.*K_ax(end)./4,'--r','Displayname','$$\frac{\omega k_0}{4}$$')
	plot(kh0,beta_hat22,'*k','Displayname','None' )
	hold off
	ylabel('$$\hat{\beta}_{22}$$','interpreter','latex')
	xlabel('kh','interpreter','latex')
	% legend('interpreter','latex')
	xlim([0.5*kh0,kh0*1.5])
	ytickformat('%,.2f')
	set(gca,'ycolor','r') 
	
	subplot(234)
	hold on
	plot(khaxis,HHat3_ax,'b','Displayname','$$\hat{H}_3$$')
	plot(kh0,Hhat3,'*k','Displayname','None' )
	plot(khaxis,on.*Omega.*K_ax(end)./2,'--b')
	hold off
	ylabel('$$\hat{H}_3$$','interpreter','latex');ytickformat('%,.1f');
	set(gca,'ycolor','b'); 
	yyaxis right
	hold on
	plot(khaxis,Alpha_hat3_ax,'r','Displayname','$$\hat{\alpha}_{3}$$')
	plot(khaxis,on.*Omega./(16.*K_ax(end).^3),'--r','Displayname','$$\frac{\omega}{16k_0^3}$$')
	plot(kh0,alpha_hat3,'*k','Displayname','None' )
	hold off
	ylabel('$$\hat{\alpha}_{3}$$','interpreter','latex')
	xlabel('kh','interpreter','latex')
	ytickformat('%,.2f')
	set(gca,'ycolor','r') 
	% legend('interpreter','latex')
	xlim([0.5*kh0,kh0*1.5])
	
	subplot(235)
	hold on
	plot(khaxis,K_ax,'b','Displayname','$$k$$')
	plot(khaxis,on.*Omega.^2,'--b','Displayname','$$k$$')
	plot(kh0,k0,'*k','Displayname','None' )
	hold off
	xlabel('kh','interpreter','latex')
	ylabel('$$k[m^{-1}]$$','interpreter','latex');	ytickformat('%,.2f')
	yyaxis right
	hold on
	plot(khaxis,H_ax,'r','Displayname','$$H$$')
	plot(kh0,h0,'*k','Displayname','None' )
	hold off
	ylabel('$$h[m]$$','interpreter','latex')
	% legend('interpreter','latex')
	xlim([0.5*kh0,kh0*1.5])
	ytickformat('%,.2f')
	set(gca,'ycolor','r') 
	
	subplot(233)
% 	semilogy(khaxis,HHat3_ax./(h0.*Bhat_D_ax),'linewidth',2)
	bhat=Bhat_D_ax-HHat3_ax./H_ax;
	bhat0=bhat_D-Hhat3./h0;
	hold on
	plot(khaxis,bhat,'linewidth',2)
	plot(kh0,bhat0,'*k','linewidth',2,'Displayname','None' )
	plot(1.363,0,'*r','linewidth',2,'Displayname','None' )
% 	plot([1.363 1.363],[-10 10],'--k','Displayname','None' )
	plot([0 khaxis(end)],[0 0],'-k','Displayname','None' )
	hold off
% 	set(gca, 'YScale', 'log')
	title('$$\hat{\beta}=\hat{\beta}_D-\hat{H}_{3}/h$$','interpreter','latex')
	xlim([0.5*kh0,kh0*1.5])
	if kh0<3
		ylim([-250,150])
	end
	xlabel('kh','interpreter','latex')

	a0=1;
	T0 = abs(1./(a0.^2.*bhat));
	% space scale
	L0 = sqrt(abs(2*Alpha_hat_ax./bhat))./a0;
	subplot(236)
	hold on
	plot(khaxis,T0,'b','Displayname','$$a_0^2 T_0$$')
	plot(khaxis,on.*abs(1./(Omega.*(K_ax(end).^2)./2)),'--b','Displayname','$$deepwater$$')
% 	plot(kh0,k0,'*k','Displayname','None' )
	hold off
	xlabel('kh','interpreter','latex')
	ylabel('$$a_0^2 T_0$$','interpreter','latex');	ytickformat('%,.2f')
	yyaxis right
	hold on
	plot(khaxis,L0,'r','Displayname','$$a0 L0$$')
	hold off
	ylabel('$$a0 L0$$','interpreter','latex')
	% legend('interpreter','latex')
	xlim([0.5*kh0,kh0*1.5])
	ytickformat('%,.2f')
	set(gca,'ycolor','r') 


% 	suptitle(strcat('Parameters $$\omega=Omega$$',num2str(Omega,'%.2f'),'$$ kh= $$', num2str(kh0,'%.2f')),'interpreter','latex','fontsize',10);
	sgtitle(strcat('Parameters $$\omega =  $$',num2str(Omega,'%.2f'),' $$ kh= $$', num2str(kh0,'%.2f')),'interpreter','latex');


end