function []=plot_params_general(X,H,K,Mu,Cp,Cg,Muintexp,Nu,Lambda,Alpha,Alpha4,Beta,Gamma)


figure;
subplot(221);
plot(X,H,X,Mu,'LineWidth',1.5);
% title('Pause, press any key to continue...','FontSize',20)
legend({'h [m]','\mu [m^{-1}]'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(422);
plot(X,K,X,Cp./Cg,X(2:end),Muintexp(2:end)./Muintexp(1:end-1),'LineWidth',1.5);
legend({'k [m^{-1}]','c_p/c_g','exp(\int \mu)'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(223);
plot(X,Nu./Lambda,'LineWidth',1.5);
legend({'\nu/ \lambda'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);
subplot(224);
plot(X,Alpha,X,Alpha4,X,Beta,X,Gamma,'LineWidth',1.5);
legend({'\alpha (TOD)','\alpha_4 (FOT)','\beta','\gamma'},'FontSize',14)
xlabel('X [m]','FontSize',14); set(gca,'Fontsize',16);