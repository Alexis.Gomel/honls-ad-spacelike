function fig=plot_simulated_gauge_fourier_stokes(k0,esteep0,Omega_dim,omegaxis,pos_gau,X,t,L,u,tplot)

g0=9.81;
fig = figure('name','Fourier signals')
    stokes_amp=0.5*esteep0^2/k0;
    pos_error=zeros(length(pos_gau),1);
    title('Simulated Fourier gauge signals','FontSize',15)
    plot_factor=20;  
    wmod=2*pi*Omega_dim;
    Omega = Omega_dim*(2*pi)/sqrt(g0);

    hold on
    for j=1:length(pos_gau)
    pos_gau(j);
    [ d, ix ] = min( abs( X - pos_gau(j) ) );%find the critical value index
    if j==1
        ix1=ix;
	end
	
    X(ix);
    pos_error(j)=X(ix)-pos_gau(j);
    ix0 = find(tplot==1,1,'first');
    fa=1.127/((t(2)-t(1)));%why is it not right???????
    f = fa*(0:(L/2))/L;
%Define the frequency domain f and plot the single-sided amplitude spectrum P1. The amplitudes are not exactly at 0.7 and 1, as expected, because of the added noise.
% On average, longer signals produce better frequency approximations.
	spectrum= fftshift(ifft(u(:,ix),[],1),1);
	P4=spectrum;
% 	[ d, ix ] = min( abs( abs(spectrum(:,1)) - max_spec ) );%find the critical value index
% 	spectrum(ix,:)=0.5*spectrum(ix-1,:)+0.5*spectrum(ix+1,:);
 



% 	P3= abs(ifft( real(u(:,ix)'.*exp(1i*2*pi*Omega_dim.*(t/sqrt(g0)-t(ix0)/sqrt(g0))))) /L ) ;  
%     P4 = P3(1:L/2+1);
%     P4(2:end-1) = 2*P4(2:end-1);
    if j==1
      norm_four=max(P4);
    end
%     l1=plot(f*sqrt(g0),1.5*P1/max(P1)+X(ix),'--r');
    l2=plot(omegaxis*sqrt(g0)/(2*pi)+Omega*sqrt(g0)/(2*pi),7*abs(P4)/norm_four+pos_gau(j),'-r','linewidth',2);
    set(l2,'LineWidth',.8)

    end  

hold off
xlim([0,4])
% title('Evolution of the freq.','Interpreter','latex')
xlabel('f (Hz)','Interpreter','latex')    
end