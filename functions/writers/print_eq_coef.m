function print_eq_coef(NLS_dispersion,NLS_nonlinear,HO_dispersion,HO_current,HO_asymetry,Hilb_coef,FOD)
filename='default_coef_equation_temp.txt';
fileID = fopen(filename,'w');

datetime = clock;
fprintf(fileID,'Parameters for button dialog case 1\n');
fprintf(fileID,'Date\t%4d.%2d.%2d\tLocal Time\t%2d:%2d:%.3f\n\n',datetime);

fprintf(fileID,'NLS_Dispersion\t%.1f\n',NLS_dispersion);
fprintf(fileID,'NLS_nonlinear\t%.1f\n',NLS_nonlinear);
fprintf(fileID,'HO_dispersion\t%.1f\n',HO_dispersion);
fprintf(fileID,'HO_current\t%.1f\n',HO_current);
fprintf(fileID,'HO_asymmetry\t%.1f\n',HO_asymetry);
fprintf(fileID,'Dythe_Hilbert\t%.1f\n',Hilb_coef);
fprintf(fileID,'HO_4th_dispersion\t%.1f\n',FOD);

fclose(fileID);
end
